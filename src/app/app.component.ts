import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import * as moment from 'moment';
//import 'moment/min/locales';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'calendar-slabcode';

  public week: any []
  public monthSelect: any[];
  public dateSelect: any;
  public dateValue: any;
  public showMon: string;

  public hours: any[];
  public cities: any[];
  public colors: any[];

  public formGroup: FormGroup;

  constructor (
    private formBuilder: FormBuilder,
  ){
    this.week = [
      "Lunes",
      "Martes",
      "Miercoles",
      "Jueves",
      "Viernes",
      "Sabado",
      "Domingo"
    ];

    this.hours = [
      '1:00 a.m.',
      '2:00 a.m.',
      '3:00 a.m.',
      '4:00 a.m.',
      '5:00 a.m.',
      '6:00 a.m.',
      '7:00 a.m.',
      '8:00 a.m.',
      '9:00 a.m.',
      '10:00 a.m.',
      '11:00 a.m.',
      '12:00 a.m.',
      '1:00 p.m.',
      '2:00 p.m.',
      '3:00 p.m.',
      '4:00 p.m.',
      '5:00 p.m.',
      '6:00 p.m.',
      '7:00 p.m.',
      '8:00 p.m.',
      '9:00 p.m.',
      '10:00 p.m.',
      '11:00 p.m.',
      '12:00 p.m.',

    ];

    this.cities = [
      'Bogotá',
      'Medellin',
      'Cali',
    ];

    this.colors = [
      'red',
      'green',
      'yellow',
    ];
  }

  ngOnInit(): void {
    this.loadForm();
    this.getDaysFromDate(3, 2021)
  }

  private loadForm() {
    this.formGroup = this.formBuilder.group({
      day: ['', [Validators.required]],
      reminder: ['', [Validators.required]],
      hour: ['', [Validators.required]],
      city: ['', [Validators.required]],
      color: ['', [Validators.required]],
    });

  }

  private getDaysFromDate(month: number, year: number): any {
    const startDate = moment.utc(`${year}/${month}/01`,  ["MM-DD-YYYY", "YYYY-MM-DD"]);
    const endDate = startDate.clone().endOf('month');
    this.dateSelect = startDate.locale('es');
    this.showMon = this.dateSelect.format('MMMM').toUpperCase() + ", " + this.dateSelect.format('yyyy');

    const diffDays = endDate.diff(startDate, 'days', true);
    const numberDays = Math.round(diffDays);

    const arrayDays = Object.keys([...Array(numberDays)]).map((a: any) => {
      a = parseInt(a) + 1;

      const dayObject = moment(`${year}-${month}-${a}`,  ["MM-DD-YYYY", "YYYY-MM-DD"]);
      return {
        name: dayObject.format("dddd"),
        value: a,
        indexWeek: dayObject.isoWeekday()
      };
    });

    this.monthSelect = arrayDays;

  }

  public changeMonth(flag: any) {
    if (flag < 0) {
      const prevDate = this.dateSelect.clone().subtract(1, "month");
      this.getDaysFromDate(prevDate.format("MM"), prevDate.format("YYYY"));
    } else {
      const nextDate = this.dateSelect.clone().add(1, "month");
      this.getDaysFromDate(nextDate.format("MM"), nextDate.format("YYYY"));
    }
  }

  public clickDay(day: any) {

    const f1: FormGroup = this.formGroup;
    f1.reset();
    const disableEnableOptions = { emitEvent: false };

    f1.get('day').setValue(day.value, disableEnableOptions);

  }

  public newReminder() {
    const f1: FormGroup = this.formGroup;

    if(f1.valid) {
      const day = f1.get('day').value - 1;
      if(this.monthSelect[day].redmi == undefined) {
        this.monthSelect[day].redmi = [];
      }

      const reminder = f1.get('reminder').value + " " + f1.get('hour').value + " " + f1.get('city').value;

      const data = {
        note: reminder,
        color: f1.get('color').value
      };

      this.monthSelect[day].redmi.push(data);
    }


  }

  public deleteReminder() {
    const f1: FormGroup = this.formGroup;
    const day = f1.get('day').value - 1;
    this.monthSelect[day].redmi = [];
  }
}
